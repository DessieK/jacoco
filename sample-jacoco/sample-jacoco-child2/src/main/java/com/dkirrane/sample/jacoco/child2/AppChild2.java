package com.dkirrane.sample.jacoco.child2;

/**
 *
 */
public class AppChild2 {

    public AppChild2() {
    }

    public String testedMethod() {
        return "testedMethod";
    }

    public String untestedMethod() {
        return "untestedMethod";
    }

    public String itTestedMethod() {
        return "itTestedMethod";
    }
}

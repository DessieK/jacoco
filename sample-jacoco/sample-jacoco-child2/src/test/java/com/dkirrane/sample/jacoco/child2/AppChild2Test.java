package com.dkirrane.sample.jacoco.child2;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class AppChild2Test {

    public AppChild2Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTestedMethod() {
        AppChild2 instance = new AppChild2();
        String expResult = "testedMethod";
        String result = instance.testedMethod();
        assertEquals(expResult, result);
    }
}

package com.dkirrane.sample.jacoco.child2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 *
 */
public class AppChild2IT {

    @Test
    public void testItTestedMethod() {
        AppChild2 instance = new AppChild2();
        String expResult = "itTestedMethod";
        String result = instance.itTestedMethod();
        assertEquals(expResult, result);
    }
}

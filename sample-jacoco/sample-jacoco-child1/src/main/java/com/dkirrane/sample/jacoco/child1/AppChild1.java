package com.dkirrane.sample.jacoco.child1;

/**
 *
 */
public class AppChild1 {

    public AppChild1() {
    }

    public String testedMethod() {
        return "testedMethod";
    }

    public String untestedMethod() {
        return "untestedMethod";
    }

    public String itTestedMethod() {
        return "itTestedMethod";
    }
    
    public String itTestedByAnotherModule() {
        return "itTestedByAnotherModule";
    }    
}

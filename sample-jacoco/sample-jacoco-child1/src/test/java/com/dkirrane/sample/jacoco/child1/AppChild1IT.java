package com.dkirrane.sample.jacoco.child1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 *
 */
public class AppChild1IT {

    @Test
    public void testItTestedMethod() {
        AppChild1 instance = new AppChild1();
        String expResult = "itTestedMethod";
        String result = instance.itTestedMethod();
        assertEquals(expResult, result);
    }
}

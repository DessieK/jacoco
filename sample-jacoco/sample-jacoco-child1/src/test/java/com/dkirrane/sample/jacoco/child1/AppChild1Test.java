package com.dkirrane.sample.jacoco.child1;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class AppChild1Test {

    public AppChild1Test() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTestedMethod() {
        AppChild1 instance = new AppChild1();
        String expResult = "testedMethod";
        String result = instance.testedMethod();
        assertEquals(expResult, result);
    }
}
